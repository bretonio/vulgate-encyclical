## A Vuetiful New Tux

### Free-Wheeling Nuxt [Vuetify on Nuxt](https://github.com/Darkside73/vuetify-nuxt-app-theme)

>Pushed by @Darkside and cloned by @bretonio. Essays in theming and global CSS for Nuxt. Not to be mistaken for [this NPM module](https://www.npmjs.com/package/@nuxtjs/vuetify) &mdash; currently also under study in our Sambodian space lab.

## First, Buildung Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

### Next, Configure Themes

> Configuring global CSS in `nuxt.config.js` with an `app.styl` file reading partially as follows.

```
@import '~vuetify/src/stylus/settings/_colors'

$theme := {
  primary:     $blue-grey.darken-2
  accent:      '#ffffe0'
  secondary:   '#757575'
  info:        $blue.darken-5
  warning:     $amber.darken-4
  error:       $red.accent-4
  success:     $green
}

@require '~vuetify/src/stylus/main.styl'
```

Or can we also set this scheme in `plugins/vuetify.js`? Yes.

```
import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  theme: {
    primary: '#607d8b',
    secondary: '#b0bec5',
    accent: '#8c9eff',
    error: '#b71c1c',
		info: #6699cc
  }
})
```

### Next, [Nested Routes](https://nuxtjs.org/examples/nested-routes/)

>Work in progress: nuances of the Nuxt router. Preferred linking syntax is

```
<template>
  <nuxt-link to="/">Home page</nuxt-link>
</template>

```

and it works.

I managed to set `active-class` to the `primary` setting in `v-list > v-list-tile` in the drawer.

### Next, All But The Kitchen Sink

> Components au gogo. Work in progress. Detail freakiness: set global flex variable to center my cards consistently and in keeping with the [Golden Mean](https://uxmovement.com/content/applying-the-golden-ratio-to-layouts-and-rectangles/).

For detailed explanation and the  defaut yada-yada on how the basics work, check out the [Nuxt.js](https://github.com/nuxt/nuxt.js) and [Vuetify.js](https://vuetifyjs.com/) documentation.
