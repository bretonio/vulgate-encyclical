import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  theme: {
    primary: '#607d8b',
    secondary: '#6699cc',
    accent: '#800080',
    error: '#990000',
    info: '#ffffE0'
  }
})
